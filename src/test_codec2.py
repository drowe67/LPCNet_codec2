#!/usr/bin/python3

import lpcnet
import sys
import numpy as np
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint
from ulaw import ulaw2lin, lin2ulaw
import keras.backend as K
import h5py

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import matplotlib.pyplot as plt

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.2
set_session(tf.Session(config=config))

output_file_prefix=sys.argv[1]
epoch = int(sys.argv[2])
feature_file = sys.argv[3]
print(epoch, feature_file)
fpcm_out = open(sys.argv[4], "wb")
frame_size = 80
nb_features = 23
nb_used_features = lpcnet.nb_used_features

features = np.fromfile(feature_file, dtype='float32')
features = np.resize(features, (-1, nb_features))
nb_frames = 1
feature_chunk_size = features.shape[0]
pcm_chunk_size = frame_size*feature_chunk_size

print(features.shape)

features[:,10:11] = np.log10(0.25*features[:,10:11]) # want log energy

# normalise to match training features

means = np.loadtxt(output_file_prefix+'_means.txt', dtype=float)
stds = np.loadtxt(output_file_prefix+'_stds.txt', dtype=float)

features[:,:11] -= means
features[:,:11] /= stds
print(means)
print(stds)
features = np.reshape(features, (nb_frames, feature_chunk_size, nb_features))

Wo = features[:,:,11:12]
periods = (2*np.pi/Wo).astype('int16')
features[:,:,11:12] = 0

lpc_order = 10

model, enc, dec = lpcnet.new_model(lpc_order)
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['sparse_categorical_accuracy'])
weight_file = output_file_prefix+'_%02d.h5' % epoch
print(weight_file)
model.load_weights(weight_file)

pcm = np.zeros((nb_frames*pcm_chunk_size, ))
fexc = np.zeros((1, 1, 2), dtype='float32')
iexc = np.zeros((1, 1, 1), dtype='int16')
state1 = np.zeros((1, lpcnet.rnn_units1), dtype='float32')
state2 = np.zeros((1, lpcnet.rnn_units2), dtype='float32')

mem = 0
coef = 0.85
pcm_out = np.zeros(0)
skip = lpc_order + 1
print("nb_frames: %d feature_chunk_size: %d" % (nb_frames, feature_chunk_size))
for c in range(0, nb_frames):
    cfeat = enc.predict([features[c:c+1, :, :nb_used_features], periods[c:c+1, :, :]])
    for fr in range(0, feature_chunk_size):
        f = c*feature_chunk_size + fr
        a = features[c, fr, nb_features-lpc_order:]
        for i in range(skip, frame_size):
            pred = -sum(a*pcm[f*frame_size + i - 1:f*frame_size + i - lpc_order-1:-1])
            fexc[0, 0, 1] = lin2ulaw(pred)

            p, state1, state2 = dec.predict([fexc, iexc, cfeat[:, fr:fr+1, :], state1, state2])
            #Lower the temperature for voiced frames to reduce
            p *= np.power(p, np.maximum(0, 1.5*features[c, fr, 12] - .5))
            p = p/(1e-18 + np.sum(p))
            #Cut off the tail of the remaining distribution
            p = np.maximum(p-0.002, 0).astype('float64')
            p = p/(1e-8 + np.sum(p))

            iexc[0, 0, 0] = np.argmax(np.random.multinomial(1, p[0,0,:], 1))
            pcm[f*frame_size + i] = pred + ulaw2lin(iexc[0, 0, 0])
            fexc[0, 0, 0] = lin2ulaw(pcm[f*frame_size + i])
            pcm[f*frame_size + i].astype(np.int16).tofile(fpcm_out)
            pcm_out = np.append(pcm_out, pcm[f*frame_size + i]);
        skip = 0
        #print(pcm_out.shape,pcm_out.dtype)
        if (fr % 50) == 0:
            plt.clf
            plt.plot(pcm_out,'b')
            plt.show(block=False)
            plt.pause(0.05)
            
        
fpcm_out.close()
