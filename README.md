# LPCNet Codec 2

Experimental Codec 2 synthesis with [LPCNET](https://github.com/mozilla/lpcnet) designed by [Jean-Marc
Valin](https://jmvalin.ca/)

Unquantised Codec 2 models are used as 13 features for LPCNet: 10 LSPs, LPC Energy, Pitch, Voicing, running at Fs=8kHz on 10ms frames.  As the LPCs are only available at Fs=8kHz, we run the system at 8kHz rather that 16kHz like most other *Net speech synthesis systems.  It could be adapted to 16 kHz synthesis if the filter represented by the LPCs was resampled to 16 kHz.  At this stage the features are unquantised, but representative of the quantised Codec 2 features.

# Quickstart

1. Set up a Keras system with GPU.

1. Build [codec2-dev](http://svn.code.sf.net/p/freetel/code/freedv-dev/README.txt)

1. Given a 16 kHz training file all_speechcat.sw:
  ```
  $ ./codec2-dev/build_linux/unittest/16_8_short all_speechcat.sw all_speechcat_8k.sw 
  $ ./codec2-dev/build_linux/src/c2sim all_speechcat_8k.sw --lspEWov all_speechcat_8k.f32 --ten_ms_centre all_speechcat_centre_8k.sw
  $ ./train_codec2.py ~/Downloads/all_speechcat_8k.f32 ~/Downloads/all_speechcat_centre_8k.sw speechcat1
  ```

  The f32 file contains the Codec 2 features, the *centre.sw file are the speech samples time aligned with the features.  The "speechcat1" argument is is the prefix of the weights files that are written to disk with each epoch, and can be any useful prefix.
  
1. You can synthesise speech with:
  ```
  $ ./codec2-dev/build_linux/src/c2sim speech_orig_8k.sw --lspEWov speech_orig_8k.f32 
  $ ./test_codec2.py speechcat1 20 ~/Downloads/speech_orig_8k.f32 speech_orig_out_8k.sw
  ```

  The "speechcat1 20" arguments mean the weights file from the 20th training epoch

# Training Tips

I initially trained with a 53Mbyte (at Fs=8kHz) file derived from the McGill TSP database of speech samples.  This worked OK with samples from within the training database, which indicated the model was OK.

However it had poor results with samples from outside the training data.  This suggests not enough training data.  The LPCNet model at the time of writing has 1.2E6 parameters, so I was training with (53E6/2 samples)/(1.2E6 weights) = 23 samples/weight.  I augmented the training data with an old set of TIMIT samples, then resampling with sox using common filters that speech signals encounter, for example:

```
 $ sox -t .sw -r 8000 -c 1 all_speech_train.sw -t .sw all_speech_train_hp.sw highpass 300
 $ sox -t .sw -r 8000 -c 1 all_speech_train.sw -t .sw all_speech_train_treble.sw gain -12 treble 12
 etc ...
```

Then combining into one big file:

```
 $ sox -t.sw -r 8000 -c 1 all_speech_train.sw -t .sw all_speech_train_hp.sw -t .sw all_speech_train_lp.sw -t .sw all_speech_train_bass.sw -t .sw all_speech_train_treble.sw all_speech_train_comb.sw
```

This gave me a 372M training file, which is (372E6/2)/1.2E6 = 155 samples/weight.  After about 20 epochs the training metrics (loss and sparse_categorical_accuracy) bottomed out.  On testing synthesis it sounded pretty good on samples inside and outside the training database.  

# Reading Further

1. [LPCNet](https://github.com/mozilla/lpcnet)

1. [WaveNet and Codec 2](https://www.rowetel.com/?p=5966)
